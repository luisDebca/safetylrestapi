package ues.edu.sv.safetylrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class SafetylRestApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SafetylRestApiApplication.class, args);
    }

}
