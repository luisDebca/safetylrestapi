package ues.edu.sv.safetylrestapi.util;

public class InvalidBearerTokenFormatException extends RuntimeException {

    public InvalidBearerTokenFormatException() {
    }

    public InvalidBearerTokenFormatException(String message) {
        super(message);
    }
}
