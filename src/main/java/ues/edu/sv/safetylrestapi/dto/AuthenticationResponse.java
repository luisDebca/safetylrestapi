package ues.edu.sv.safetylrestapi.dto;

import lombok.Data;

@Data
public class AuthenticationResponse {

    private final String jwt;
}
