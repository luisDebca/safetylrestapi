package ues.edu.sv.safetylrestapi.controller;

import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ues.edu.sv.safetylrestapi.dto.ResponseDTO;
import ues.edu.sv.safetylrestapi.filter.TokenOnBlackListException;

@Log4j2
@ControllerAdvice
public class ExceptionHelperController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {ExpiredJwtException.class})
    protected ResponseEntity<ResponseDTO> handleExpiredJwtException(RuntimeException e, WebRequest request) {
        log.warn("Jwt token expired, returning info with error.");
        return new ResponseEntity<>(new ResponseDTO("Jwt token expired...", "401", null, null), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(value = {TokenOnBlackListException.class})
    protected ResponseEntity<ResponseDTO> handleTokenBlockedException(RuntimeException e, WebRequest request) {
        log.warn("Jwt token blocked, returning info with error.");
        return new ResponseEntity<>(new ResponseDTO("Jwt token blocked.", "401", null, null), HttpStatus.UNAUTHORIZED);
    }
}
