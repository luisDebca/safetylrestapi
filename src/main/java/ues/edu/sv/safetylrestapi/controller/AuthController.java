package ues.edu.sv.safetylrestapi.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ues.edu.sv.safetylrestapi.dto.AuthenticationRequest;
import ues.edu.sv.safetylrestapi.dto.AuthenticationResponse;
import ues.edu.sv.safetylrestapi.dto.ResponseDTO;
import ues.edu.sv.safetylrestapi.service.CustomUserDetailsService;
import ues.edu.sv.safetylrestapi.util.HttpRequestUtil;
import ues.edu.sv.safetylrestapi.util.InvalidBearerTokenFormatException;
import ues.edu.sv.safetylrestapi.util.JwtUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api/auth")
@Log4j2
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private CustomUserDetailsService userDetailsService;

    @Autowired
    private JwtUtil jwtUtil;

    @PostMapping("/authenticate")
    public ResponseEntity<?> login(@RequestBody AuthenticationRequest authenticationRequest) {
        try {
            log.info("Authenticating user...");
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword()));
            log.info("Authentication completed !!!");
        } catch (AuthenticationException e) {
            log.error("User name or password incorrect...");
            return new ResponseEntity<>(new ResponseDTO("Incorrect username or password", "401", null, e.getCause()), HttpStatus.UNAUTHORIZED);
        }
        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        log.info("Generating jwt response...");
        final String jwt = jwtUtil.generateToken(userDetails);
        log.info("Returning jwt");
        return ResponseEntity.ok(new AuthenticationResponse(jwt));
    }

    @PostMapping("/logout")
    public ResponseEntity<ResponseDTO> disableToken(HttpServletRequest request, HttpServletResponse response) {
        try {
            if (HttpRequestUtil.requestNotContainsAuthentication(request)) {
                return new ResponseEntity<>(new ResponseDTO("Request without token...", "400", null, null), HttpStatus.BAD_REQUEST);
            } else {
                String jwt = HttpRequestUtil.getAuthenticationToken(request);
                jwtUtil.addTokenToBlacklist(jwt);
            }
            ResponseDTO responseDTO = new ResponseDTO("OK", "200", null, null);
            return new ResponseEntity<>(responseDTO, HttpStatus.OK);
        } catch (InvalidBearerTokenFormatException e) {
            return new ResponseEntity<>(new ResponseDTO("Invalid Token format.", "400", null, e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }
}
